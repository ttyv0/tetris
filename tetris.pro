TEMPLATE = app
TARGET = tetris
DEPENDPATH += . include src include/SDL
INCLUDEPATH += . src include/SDL include
CONFIG += console
CONFIG -= qt
unix:LIBS += -L$$IN_PWD/lib_linux -lSDL -lSDL_image -lSDL_ttf -lSDL_draw
win32:LIBS += -static-libstdc++ -Llib_windows -lSDL -lSDL_image -lmingw32 -lSDLmain -lSDL_draw -lSDL_ttf

# Input
HEADERS += include/SDL_draw.h src/tetris.h include/SDL/SDL_ttf.h
SOURCES += src/blocks.cpp \
           src/check.cpp \
           src/main.cpp \
           src/newblock.cpp \
           src/output.cpp \
           src/positioning.cpp
