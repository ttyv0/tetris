#include <time.h>
#include "tetris.h"
#include <SDL/SDL_ttf.h>
#include "../include/SDL_draw.h"
extern "C" void srand(unsigned int);
Uint32 mytimers(Uint32, void *param);
tetris::tetris(){
for (register short j=0; j<24;j++){
	for (register short i=0; i<10;i++)
	field[i][j]=0;
}
	srand(time(0));
	nb=1;
	bnum1=-1;
	score=0;
	level=0;
if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0 ){
printf("Unable to init SDL: %s\n", SDL_GetError());
exit(1);
}
SDL_WM_SetCaption("Tetris",NULL);
screen = SDL_SetVideoMode(518,643,0,SDL_HWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL ){
		printf("Unable to set 518x643 video: %s\n", SDL_GetError());
		exit(1);
	}
//color init begin
black = SDL_MapRGB(screen->format, 0x00, 0x00, 0x00);
green1 = SDL_MapRGB(screen->format, 0x0e, 0xa0, 0x2f);
green2 = SDL_MapRGB(screen->format, 0x0c, 0xde, 0x25);
green3 = SDL_MapRGB(screen->format, 0x02, 0x57, 0x0c);
//color init end

//surface init begin
img = SDL_CreateRGBSurface(SDL_SWSURFACE,32,32,32,0x00FF0000,0x0000FF00,0x000000FF,0x00000000);
fieldsurface = SDL_CreateRGBSurface(SDL_SWSURFACE,320,640,32,0x00FF0000,0x0000FF00,0x000000FF,0x00000000);
minifieldsurface = SDL_CreateRGBSurface(SDL_SWSURFACE,128,128,32,0x00FF0000,0x0000FF00,0x000000FF,0x00000000);
//surface init end

//draw block begin
Draw_FillRect(img, 2, 2, 28, 28, green1);
Draw_Rect(img, 0, 0, 32, 32, black);
Draw_Rect(img, 1, 1, 30, 30, green3);
Draw_Line(img, 3, 2, 29, 28, green3);
Draw_Line(img, 2, 2, 29, 29, green3);
Draw_Line(img, 2, 3, 28, 29, green3);
Draw_Line(img, 28, 2, 2, 28, green3);
Draw_Line(img, 29, 2, 2, 29, green3);
Draw_Line(img, 29, 3, 3, 29, green3);
Draw_FillRect(img, 8, 8, 16, 16, green2);
//draw block end

//draw const interface
Draw_Rect(screen, 351, 31, 130, 130, green1);
Draw_Rect(screen, 0, 0, 322, 642, green1);

TTF_Init();
SDL_Color clr = {14,160,47,0};
fnt = TTF_OpenFont("fonts/UrsulaLight.ttf", 18);
if ( fnt == NULL ){
	printf("Font is not load");
	exit(1);
}
//TTF_Cache
char c[2];
for ( short i=0; i<10;i++){
snprintf(c, sizeof(c), "%i", i);
ttfcache[i] = TTF_RenderText_Blended(fnt, c, clr);
}
ttfcache[10] = TTF_RenderText_Blended(fnt, "Level:", clr);
ttfcache[11] = TTF_RenderText_Blended(fnt, "Score:", clr);
//
ttfcache[12] = SDL_CreateRGBSurface(SDL_SWSURFACE, ttfcache[0]->w*8,ttfcache[0]->h,32,0x00FF0000,0x0000FF00,0x000000FF,0x00000000);
//
DrawScore();
//
SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
SDL_WM_SetIcon(img, NULL);
}
tetris::~tetris(){
	SDL_FreeSurface(screen);
	SDL_FreeSurface(img);
	SDL_FreeSurface(fieldsurface);
	SDL_FreeSurface(minifieldsurface);
	for (int i=0; i<13;i++)
	SDL_FreeSurface(ttfcache[i]);
	TTF_CloseFont(fnt);
	TTF_Quit();
	SDL_Quit();
}
tetris t;
int main(){
t.newblock(0);
SDL_Event event;
SDL_AddTimer(1000, mytimers, NULL);
	while ( SDL_WaitEvent(&event) ){
	t.field_out();
		if ( event.type == SDL_QUIT ){ return 0; }
		if ( event.type == SDL_KEYDOWN ){
			if ( event.key.keysym.sym == SDLK_ESCAPE or event.key.keysym.sym == 'q'){ break; }
			if ( event.key.keysym.sym == SDLK_LEFT or event.key.keysym.sym =='a'){ t.left(); }
			if ( event.key.keysym.sym == SDLK_RIGHT or event.key.keysym.sym =='d'){ t.right(); }
			if ( event.key.keysym.sym == SDLK_UP or event.key.keysym.sym =='w'){ t.newblock(1); }
			if ( event.key.keysym.sym == SDLK_DOWN or event.key.keysym.sym =='s'){ if (t.down()) { t.delete_line(); if (t.check_gameover()) break; t.newblock(0); }}
		}
		if ( event.type == SDL_USEREVENT ){
			if (t.down()) { t.delete_line(); if (t.check_gameover()) break; t.newblock(0); }

		}
	}
//score screen
t.scorescreen();
while ( SDL_WaitEvent(&event) ){
	if ( event.type == SDL_QUIT ){ return 0; }
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == SDLK_RETURN or event.key.keysym.sym == SDLK_KP_ENTER) { break; }
}
return 0;
}

Uint32 mytimers(Uint32 delay, void*){
SDL_Event event_down;
event_down.type=SDL_USEREVENT;
SDL_PushEvent(&event_down);
return 1000-t.level*100;
}
