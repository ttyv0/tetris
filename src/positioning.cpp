#include "tetris.h"
void tetris::left()
{
	for (register short i=0;i<4;i++)
	field[coordinates[0][i]][coordinates[1][i]]=0;

	for (register short i=0;i<4;i++)
	if ( coordinates[0][i]-1 < 0 or field[coordinates[0][i]-1][coordinates[1][i]]==1)
		{
		for (register short j=0;j<4;j++)
		field[coordinates[0][j]][coordinates[1][j]]=1;
		return;
		}	

	for (register short i=0;i<4;i++)
	field[coordinates[0][i]=coordinates[0][i]-1][coordinates[1][i]]=1;
}
void tetris::right()
{
	for (register short i=0;i<4;i++)
	field[coordinates[0][i]][coordinates[1][i]]=0;
	
	for (register short i=0;i<4;i++)
	if ( coordinates[0][i]+1 > 9 or field[coordinates[0][i]+1][coordinates[1][i]]==1)
		{
		for (register short j=0;j<4;j++)
		field[coordinates[0][j]][coordinates[1][j]]=1;
		return;
		}	
	
	
	for (register short i=0;i<4;i++)
	field[coordinates[0][i]=coordinates[0][i]+1][coordinates[1][i]]=1;
}
bool tetris::down()
{
	for (register short i=0;i<4;i++)
	field[coordinates[0][i]][coordinates[1][i]]=0;

	for (register short i=0;i<4;i++)
	if ( coordinates[1][i]-1 < 0 or field[coordinates[0][i]][coordinates[1][i]-1]==1)
		{
		for (register short j=0;j<4;j++)
		field[coordinates[0][j]][coordinates[1][j]]=1;
		return 1;
		}

	for (register short i=0;i<4;i++)
	field[coordinates[0][i]][coordinates[1][i]=coordinates[1][i]-1]=1;
	return 0;
}
