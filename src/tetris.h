#ifndef __TETRIS
#define __TETRIS
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

class tetris {
	bool field[10][24];
    short coordinates[2][4];
    short nb;
    void DrawSurf(SDL_Surface*, Sint16, Sint16, SDL_Surface*);
    void ClearSurf(SDL_Surface*);
    void ClearSurf(SDL_Surface*, Sint16, Sint16, Uint16, Uint16);
    void minifield_draw();
    void DrawScore();
    void line1(bool);
    void line2(bool);
	void square();
	void angel01(bool);
	void angel02(bool);
	void angel03(bool);
	void angel04(bool);
	void angel11(bool);
	void angel12(bool);
	void angel13(bool);
	void angel14(bool);
	void pyramid1(bool);
	void pyramid2(bool);
	void pyramid3(bool);
	void pyramid4(bool);
	void f1(bool);
	void f2(bool);
	void f3(bool);
	void f4(bool);
	inline short randint();
	SDL_Surface *screen;
	SDL_Surface *img;
	SDL_Surface *minifieldsurface;
	SDL_Surface *fieldsurface;
	Uint32 black;
	Uint32 green1;
	Uint32 green2;
	Uint32 green3;
	short bnum1,bnum2;
	short level;
	int score;
	SDL_Surface *ttfcache[13];
	TTF_Font *fnt;

public:
	tetris();
	~tetris();
	void field_out();
	void delete_line();
	bool check_gameover();
	void left();
	void right();
	bool down();
	void newblock(bool);
    friend Uint32 mytimers(Uint32, void*);
	void scorescreen();

};
#endif
