#include "tetris.h"
void tetris::line1(bool status)
{
if ( ! status ){
	field[3][20]=field[4][20]=field[5][20]=field[6][20]=1;
	coordinates[1][0]=coordinates[1][1]=coordinates[1][2]=coordinates[1][3]=20;
	coordinates[0][0]=3;
	coordinates[0][1]=4;
	coordinates[0][2]=5;
	coordinates[0][3]=6;
	nb=1;
	return;
}
else{
	short a = coordinates[1][0]+1;
	for (register short i =1;i<4;i++,a++){
		if (field[coordinates[0][0]][a])
		return;		
	}
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=0;
	coordinates[0][1]=coordinates[0][2]=coordinates[0][3]=coordinates[0][0];
	coordinates[1][1]=coordinates[1][0]+1;
	coordinates[1][2]=coordinates[1][1]+1;
	coordinates[1][3]=coordinates[1][2]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
		
	nb=2;
	return;
}
}

void tetris::line2(bool status){
if ( ! status ){
	field[5][23]=field[5][22]=field[5][21]=field[5][20]=1;
	coordinates[0][0]=coordinates[0][1]=coordinates[0][2]=coordinates[0][3]=5;
	coordinates[1][0]=20;
	coordinates[1][1]=21;
	coordinates[1][2]=22;
	coordinates[1][3]=23;
	nb=2;
	return;
}
else{
	short a = coordinates[0][3]+1;
	for (register short i =1;i<4;i++,a++){
		if (field[a][coordinates[1][3]] or a>9)
		return;		
	}
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=0;
	coordinates[0][1]=coordinates[0][0]+1;
	coordinates[0][2]=coordinates[0][1]+1;
	coordinates[0][3]=coordinates[0][2]+1;
	coordinates[1][1]=coordinates[1][2]=coordinates[1][3]=coordinates[1][0];
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
		
	nb=1;
	return;
}
}
void tetris::square(){
field[4][21]=field[4][20]=field[5][21]=field[5][20]=1;
coordinates[0][0]=coordinates[0][1]=4;
coordinates[1][0]=coordinates[1][2]=21;
coordinates[0][2]=coordinates[0][3]=5;
coordinates[1][1]=coordinates[1][3]=20;
}
void tetris::angel01(bool status){
if ( ! status ){
	field[3][22]=field[3][21]=field[3][20]=field[4][20]=1;
	coordinates[0][0]=coordinates[0][1]=coordinates[0][2]=3;
	coordinates[0][3]=4;
	coordinates[1][0]=22;
	coordinates[1][1]=21;
	coordinates[1][2]=coordinates[1][3]=20;
	nb=3;
	return;
}
else{
	if ( coordinates[0][0]==0 or field[coordinates[0][0]-1][coordinates[1][0]] or field[coordinates[0][0]+1][coordinates[1][0]] or field[coordinates[0][0]-1][coordinates[1][0]-1] ) return;
	field[coordinates[0][1]][coordinates[1][1]]=field[coordinates[0][2]][coordinates[1][2]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[1][1]=coordinates[1][1]+1;
	coordinates[1][2]=coordinates[1][2]+2;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[0][3]=coordinates[0][3]-2;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=4;
	return;
}
}
void tetris::angel02(bool status){
if ( ! status ){
	field[3][21]=field[4][21]=field[5][21]=field[3][20]=1;
	coordinates[0][0]=coordinates[0][3]=3;
	coordinates[0][1]=4;
	coordinates[0][2]=5;
	coordinates[1][0]=coordinates[1][1]=coordinates[1][2]=21;
	coordinates[1][3]=20;
	nb=4;
	return;
}
else{
	if ( coordinates[1][3]==0 or field[coordinates[0][1]][coordinates[1][0]-1] or field[coordinates[0][1]][coordinates[1][0]-2] ) return;
	field[coordinates[0][2]][coordinates[1][2]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][2]=coordinates[0][2]-1;
	coordinates[1][2]=coordinates[1][2]-1;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=5;
	return;
}
}
void tetris::angel03(bool status){
if ( ! status ){
	field[3][22]=field[4][22]=field[4][21]=field[4][20]=1;
	coordinates[0][0]=3;
	coordinates[0][1]=coordinates[0][2]=coordinates[0][3]=4;
	coordinates[1][0]=coordinates[1][1]=22;
	coordinates[1][2]=21;
	coordinates[1][3]=20;
	nb=5;
	return;
}
else{
	if ( coordinates[0][1]==9 or field[coordinates[0][0]][coordinates[1][0]-1] or field[coordinates[0][1]+1][coordinates[1][1]] or field[coordinates[0][1]+1][coordinates[1][1]+1] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][1]][coordinates[1][1]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]+2;
	coordinates[0][1]=coordinates[0][1]-1;
	coordinates[1][1]=coordinates[1][1]-1;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=6;
	return;
}
}
void tetris::angel04(bool status){
if ( ! status ){
	field[5][21]=field[3][20]=field[4][20]=field[5][20]=1;
	coordinates[0][0]=coordinates[0][3]=5;
	coordinates[0][1]=3;
	coordinates[0][2]=4;
	coordinates[1][0]=21;
	coordinates[1][1]=coordinates[1][2]=coordinates[1][3]=20;
	nb=6;
	return;
}
else{
	if ( coordinates[1][2]==0 or field[coordinates[0][0]-1][coordinates[1][0]] or field[coordinates[0][2]][coordinates[1][2]-1] or field[coordinates[0][3]][coordinates[1][3]-1] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][1]][coordinates[1][1]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[0][1]=coordinates[0][1]+1;
	coordinates[1][2]=coordinates[1][2]-1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=3;
	return;
}
}
void tetris::angel11(bool status){
if ( ! status ){
	field[4][22]=field[4][21]=field[4][20]=field[3][20]=1;
	coordinates[0][0]=coordinates[0][1]=coordinates[0][3]=4;
	coordinates[0][2]=3;
	coordinates[1][0]=22;
	coordinates[1][1]=21;
	coordinates[1][2]=coordinates[1][3]=20;
	nb=7;
	return;
}
else{
	if ( coordinates[0][3]==9 or field[coordinates[0][0]-1][coordinates[1][0]] or field[coordinates[0][0]+1][coordinates[1][0]-1] or field[coordinates[0][0]-1][coordinates[1][0]-1] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][2]][coordinates[1][2]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[0][1]=coordinates[0][1]-1;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[1][2]=coordinates[1][2]+1;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=8;
	return;
}
}
void tetris::angel12(bool status){
if ( ! status ){
	field[3][21]=field[3][20]=field[4][20]=field[5][20]=1;
	coordinates[0][0]=coordinates[0][1]=3;
	coordinates[0][2]=4;
	coordinates[0][3]=5;
	coordinates[1][1]=coordinates[1][2]=coordinates[1][3]=20;
	coordinates[1][0]=21;
	nb=8;
	return;
}
else{
	if ( coordinates[1][2]==0 or field[coordinates[0][0]+1][coordinates[1][0]] or field[coordinates[0][0]+2][coordinates[1][0]] or field[coordinates[0][2]][coordinates[1][2]-1] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][1]][coordinates[1][1]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]+1;
	coordinates[0][1]=coordinates[0][1]+2;
	coordinates[1][1]=coordinates[1][1]+1;
	coordinates[0][3]=coordinates[0][3]-1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=9;
	return;
}
}
void tetris::angel13(bool status){
if ( ! status ){
	field[4][22]=field[3][22]=field[3][21]=field[3][20]=1;
	coordinates[0][1]=4;
	coordinates[0][0]=coordinates[0][2]=coordinates[0][3]=3;
	coordinates[1][0]=coordinates[1][1]=22;
	coordinates[1][2]=21;
	coordinates[1][3]=20;
	nb=9;
	return;
}
else{
	if ( coordinates[0][0]==0 or field[coordinates[0][0]-1][coordinates[1][0]] or field[coordinates[0][1]][coordinates[1][1]-1] ) return;
	field[coordinates[0][2]][coordinates[1][2]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[0][1]=coordinates[0][1]-1;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[1][2]=coordinates[1][2]+1;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=10;
	return;
}
}
void tetris::angel14(bool status){
if ( ! status ){
	field[3][21]=field[4][21]=field[5][21]=field[5][20]=1;
	coordinates[0][0]=3;
	coordinates[0][1]=4;
	coordinates[0][2]=coordinates[0][3]=5;
	coordinates[1][3]=20;
	coordinates[1][0]=coordinates[1][1]=coordinates[1][2]=21;
	nb=10;
	return;
}
else{
	if ( coordinates[1][3]==0 or field[coordinates[0][0]][coordinates[1][0]-2] or field[coordinates[0][1]][coordinates[1][1]-1] or field[coordinates[0][1]][coordinates[1][1]-2] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][2]][coordinates[1][2]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]+1;
	coordinates[0][2]=coordinates[0][2]-2;
	coordinates[0][3]=coordinates[0][3]-1;
	coordinates[1][1]=coordinates[1][1]-1;
	coordinates[1][2]=coordinates[1][2]-2;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=7;
	return;
}
}
void tetris::pyramid1(bool status){
	if ( ! status ){
	field[3][21]=field[4][21]=field[5][21]=field[4][20]=1;
	coordinates[0][0]=3;
	coordinates[0][2]=5;
	coordinates[0][1]=coordinates[0][3]=4;
	coordinates[1][3]=20;
	coordinates[1][0]=coordinates[1][1]=coordinates[1][2]=21;
	nb=11;
	return;
}
else{
	if ( field[coordinates[0][1]][coordinates[1][1]+1] ) return;
	field[coordinates[0][2]][coordinates[1][2]]=0;
	coordinates[0][0]=coordinates[0][0]+1;
	coordinates[0][1]=coordinates[0][1]-1;
	coordinates[0][2]=coordinates[0][2]-1;
	coordinates[1][0]=coordinates[1][0]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=12;
	return;
}
}
void tetris::pyramid2(bool status){
	if ( ! status ){
	field[3][21]=field[4][22]=field[4][21]=field[4][20]=1;
	coordinates[0][0]=coordinates[0][2]=coordinates[0][3]=4;
	coordinates[0][1]=3;
	coordinates[1][0]=22;
	coordinates[1][1]=coordinates[1][2]=21;
	coordinates[1][3]=20;
	nb=12;
	return;
}
else{
	if ( coordinates[0][2]==9 or field[coordinates[0][2]+1][coordinates[1][2]] ) return;
	field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=13;
	return;
}
}
void tetris::pyramid3(bool status){
	if ( ! status ){
	field[4][21]=field[3][20]=field[4][20]=field[5][20]=1;
	coordinates[0][0]=coordinates[0][2]=4;
	coordinates[0][1]=3;
	coordinates[0][3]=5;
	coordinates[1][0]=21;
	coordinates[1][1]=coordinates[1][2]=coordinates[1][3]=20;
	nb=13;
	return;
}
else{
	if ( coordinates[1][2]==0 or field[coordinates[0][2]][coordinates[1][2]-1] ) return;
	field[coordinates[0][1]][coordinates[1][1]]=0;
	coordinates[0][1]=coordinates[0][1]+1;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[0][3]=coordinates[0][3]-1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=14;
	return;
}
}
void tetris::pyramid4(bool status){
	if ( ! status ){
	field[3][22]=field[3][21]=field[3][20]=field[4][21]=1;
	coordinates[0][0]=coordinates[0][1]=coordinates[0][3]=3;
	coordinates[0][2]=4;
	coordinates[1][3]=20;
	coordinates[1][0]=22;
	coordinates[1][1]=coordinates[1][2]=21;
	nb=14;
	return;
}
else{
	if ( coordinates[0][1]==0 or field[coordinates[0][1]-1][coordinates[1][1]] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[1][0]=coordinates[1][0]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=11;
	return;
}
}
void tetris::f1(bool status){
	if ( ! status ){
	field[3][21]=field[4][21]=field[4][20]=field[5][20]=1;
	coordinates[0][0]=3;
	coordinates[0][1]=coordinates[0][2]=4;
	coordinates[0][3]=5;
	coordinates[1][0]=coordinates[1][1]=21;
	coordinates[1][2]=coordinates[1][3]=20;
	nb=15;
	return;
	}
else{
	if ( coordinates[1][2]==0 or field[coordinates[0][2]][coordinates[1][2]-1] or field[coordinates[0][1]+1][coordinates[1][1]] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][1]][coordinates[1][1]]=0;
	coordinates[0][0]=coordinates[0][0]+2;
	coordinates[1][1]=coordinates[1][1]-1;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[0][3]=coordinates[0][3]-1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=16;
	return;
}
}void tetris::f2(bool status){
	if ( ! status ){
	field[4][22]=field[3][21]=field[4][21]=field[3][20]=1;
	coordinates[0][0]=coordinates[0][2]=4;
	coordinates[0][1]=coordinates[0][3]=3;
	coordinates[1][0]=22;
	coordinates[1][3]=20;
	coordinates[1][1]=coordinates[1][2]=21;
	nb=16;
	return;
	}
else{
	if ( coordinates[0][1]==0 or field[coordinates[0][0]-2][coordinates[1][0]] or field[coordinates[0][0]-1][coordinates[1][0]] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]-2;
	coordinates[1][1]=coordinates[1][1]+1;
	coordinates[0][2]=coordinates[0][2]-1;
	coordinates[0][3]=coordinates[0][3]+1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=15;
	return;
}
}void tetris::f3(bool status){
	if ( ! status ){
	field[3][20]=field[4][20]=field[4][21]=field[5][21]=1;
	coordinates[0][1]=5;
	coordinates[0][0]=coordinates[0][3]=4;
	coordinates[0][2]=3;
	coordinates[1][0]=coordinates[1][1]=21;
	coordinates[1][2]=coordinates[1][3]=20;
	nb=17;
	return;
	}
else{
	if ( coordinates[1][3]==0 or field[coordinates[0][0]-1][coordinates[1][0]] or field[coordinates[0][3]][coordinates[1][3]-1] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][1]][coordinates[1][1]]=0;
	coordinates[0][0]=coordinates[0][0]-1;
	coordinates[0][1]=coordinates[0][1]-2;
	coordinates[1][1]=coordinates[1][1]-1;
	coordinates[0][2]=coordinates[0][2]+1;
	coordinates[1][3]=coordinates[1][3]-1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=18;
	return;
}
}void tetris::f4(bool status){
	if ( ! status ){
	field[3][22]=field[3][21]=field[4][21]=field[4][20]=1;
	coordinates[0][0]=coordinates[0][1]=3;
	coordinates[0][2]=coordinates[0][3]=4;
	coordinates[1][0]=22;
	coordinates[1][1]=coordinates[1][2]=21;
	coordinates[1][3]=20;
	nb=18;
	return;
	}
else{
	if (coordinates[0][3]==9 or field[coordinates[0][0]+1][coordinates[1][0]] or field[coordinates[0][0]+2][coordinates[1][0]] ) return;
	field[coordinates[0][0]][coordinates[1][0]]=field[coordinates[0][3]][coordinates[1][3]]=0;
	coordinates[0][0]=coordinates[0][0]+1;
	coordinates[0][1]=coordinates[0][1]+2;
	coordinates[1][1]=coordinates[1][1]+1;
	coordinates[0][2]=coordinates[0][2]-1;
	coordinates[1][3]=coordinates[1][3]+1;
		for (register short i=0;i<4;i++)
		field[coordinates[0][i]][coordinates[1][i]]=1;
	nb=17;
	return;
}
}
