#include "tetris.h"
bool minifield[19][4][4]={
		{{0,0,0,0},{0,1,1,0},{0,1,1,0},{0,0,0,0}},//0
		{{0,0,0,0},{0,0,0,0},{0,0,0,0},{1,1,1,1}},
		{{0,0,1,0},{0,0,1,0},{0,0,1,0},{0,0,1,0}},
		{{0,0,0,0},{0,1,0,0},{0,1,0,0},{0,1,1,0}},
		{{0,0,0,0},{0,1,1,1},{0,1,0,0},{0,0,0,0}},
		{{0,0,0,0},{0,1,1,0},{0,0,1,0},{0,0,1,0}},//5
		{{0,0,0,0},{0,0,0,1},{0,1,1,1},{0,0,0,0}},
		{{0,0,0,0},{0,0,1,0},{0,0,1,0},{0,1,1,0}},
//very stupid solution =(
		{{0,0,0,0},{0,1,0,0},{0,1,1,1},{0,0,0,0}},
		{{0,0,0,0},{0,1,1,0},{0,1,0,0},{0,1,0,0}},
		{{0,0,0,0},{0,1,1,1},{0,0,0,1},{0,0,0,0}},//10
		{{0,0,0,0},{0,1,1,1},{0,0,1,0},{0,0,0,0}},
		{{0,0,0,0},{0,0,1,0},{0,1,1,0},{0,0,1,0}},
		{{0,0,0,0},{0,0,1,0},{0,1,1,1},{0,0,0,0}},
		{{0,0,0,0},{0,1,0,0},{0,1,1,0},{0,1,0,0}},
		{{0,0,0,0},{0,1,1,0},{0,0,1,1},{0,0,0,0}},//15
		{{0,0,0,0},{0,0,1,0},{0,1,1,0},{0,1,0,0}},
		{{0,0,0,0},{0,0,1,1},{0,1,1,0},{0,0,0,0}},
		{{0,0,0,0},{0,1,0,0},{0,1,1,0},{0,0,1,0}},
		};
void tetris::DrawSurf(SDL_Surface *surface, Sint16 x, Sint16 y, SDL_Surface *surf){
	SDL_Rect dest = {x, y, 0, 0};
	SDL_BlitSurface(surf, NULL, surface, &dest);
}
void tetris::ClearSurf(SDL_Surface *surface){
	SDL_FillRect(surface, 0, black);
}

void tetris::ClearSurf(SDL_Surface *surface, Sint16 a, Sint16 b, Uint16 c, Uint16 d){
	SDL_Rect r = {a, b, c, d};
	SDL_FillRect(surface, &r, black);
}

void tetris::field_out(){
	ClearSurf(fieldsurface);
	for (register short j=19; j>=0;j--)
		for (register short i=0; i<10;i++)
			if(field[i][j])  DrawSurf(fieldsurface, i*32, (19-j)*32, img);
minifield_draw();
DrawSurf(screen, 351, 171, ttfcache[10]);
ClearSurf(screen, 358+ttfcache[10]->w, 171, ttfcache[0]->w, ttfcache[0]->h);
DrawSurf(screen, 358+ttfcache[10]->w, 171, ttfcache[level]);
DrawSurf(screen, 351, 200, ttfcache[11]);
DrawSurf(screen, 358+ttfcache[11]->w, 200, ttfcache[12]);
DrawSurf(screen, 1, 1, fieldsurface);
DrawSurf(screen, 352, 32, minifieldsurface);
SDL_Flip(screen);

}
void tetris::minifield_draw(){
SDL_FillRect(minifieldsurface, 0, black);
for (register short j=0; j<4;j++)
		for (register short i=0; i<4;i++)
			if(minifield[bnum2][j][i])  DrawSurf(minifieldsurface, i*32, j*32, img);
}
void tetris::DrawScore(){
	ClearSurf(ttfcache[12]);
	char c[9];
	snprintf(c, sizeof(c), "%i", score);
	short strsize = strlen(c);
	for (int i=0; i<8-strsize; i++)
		DrawSurf(ttfcache[12], i*ttfcache[0]->w, 0, ttfcache[0]);
	for (int i=0; i<strsize; i++)
	DrawSurf(ttfcache[12], (8-strsize+i)*ttfcache[0]->w, 0, ttfcache[c[i]-48]);
}
void tetris::scorescreen(){
	SDL_Color clr = {14,160,47,0};
	SDL_Surface *temp = TTF_RenderText_Blended(fnt, "Press ENTER to exit.", clr);
	ClearSurf(screen);
	DrawSurf(screen, 160, 240, ttfcache[11]);
	DrawSurf(screen, 162+ttfcache[11]->w, 240, ttfcache[12]);
	DrawSurf(screen, 160, 270, temp);
	SDL_FreeSurface(temp);
	SDL_Flip(screen);
}
