#include "tetris.h"
bool tetris::check_gameover(){

for (register short i=0; i<10;i++)
	if (field[i][20]==1) return 1;
return 0;

}

void tetris::delete_line(){
short lines=0;
for (register short j=20;j>=0;j--){
short l=0;
for (register short i=0;i<10;i++){
	if (field[i][j])
		l++;
	else break;
}
	if (l==10){
		lines++;
		for (register short j1=j;j1<23;j1++)
			for (register short i1=0;i1<10;i1++)
				field[i1][j1]=field[i1][j1+1];
	}
}
if (lines==0) return;
switch(lines){
	case 1:
		score=score+5;
		break;
	case 2:
		score=score+15;
		break;
	case 3:
		score=score+30;
		break;
	case 4:
		score=score+50;
		break;
}
if ( score >= (level*2+1)*200 ) if ( level <= 8 ) level++;
DrawScore();

}
